import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import queryParser from 'query-parser-express';

import errorHandler from '../middleware/errorHandler';
import logger from '../middleware/logger';
import validator from '../middleware/validator';

export default class App {
  constructor({ basicRouter }) {
    const app = express();
    app.use(helmet());
    app.use(logger());
    app.use(cors());
    app.use(queryParser());
    app.use(validator());
    app.use(bodyParser.json());
    app.use('/api', basicRouter);
    app.use(errorHandler());
    return app;
  }
}
