export default class ConverterController {
  constructor({ converterService }) {
    this.converterService = converterService;
  }

    convert = (req, res, next) => this.converterService
      .convert({ amount: req.query.amount, from: req.query.from, to: req.query.to })
      .then(result => (res.status(200).json(result)))
      .catch(next)
}
