export default class InfoController {
  constructor({ infoService }) {
    this.infoService = infoService;
  }

    time = (req, res, next) => this.infoService
      .getServerTime()
      .then(time => (res.status(200).json(time)))
      .catch(next)
}
