import winston from 'winston';
import ElasticSearch from 'winston-elasticsearch';
import config from 'node-config-env-value';

const ENV = config.get('NODE_ENV');
const LOG_LEVEL = config.get('LOG_LEVEL');

const rewriters = [
  (level, msg, meta) => ({ ...meta, env: ENV, timeStamp: new Date() }),
];

const levels = {
  error: 0, warn: 1, state: 2, prof: 3, info: 4, debug: 5,
};

const colors = {
  error: 'red', warn: 'cyan', state: 'green', prof: 'blue', info: 'yellow', debug: 'gray',
};

winston.addColors(colors);

const transports = [];
if (ENV === 'localhost' || ENV === 'test') {
  transports.push(new winston.transports.Console({
    timestamp: true,
    colorize: true,
    level: LOG_LEVEL,
  }));
}
if (ENV === 'production' || ENV === 'development' || ENV === 'perfomance') {
  const ELASTIC_HOST = config.get('ELASTIC_HOST');
  transports.push(new ElasticSearch({
    clientOpts: {
      host: ELASTIC_HOST,
    },
    index: `time-server:${ENV}`,
    flushInterval: 500,
    json: true,
    level: LOG_LEVEL,
    ensureMappingTemplate: false,
  }));
}

export default new (winston.Logger)({
  levels,
  transports,
  rewriters,
});
