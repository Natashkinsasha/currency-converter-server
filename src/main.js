import HttpServer from './app/HttpServer';
import ConverterService from './service/ConverterService';
import App from './app/App';
import BasicRouter from './router/BasicRouter';
import InfoRouter from './router/InfoRouter';
import ConverterRouter from './router/ConverterRouter';
import InfoController from './controller/InfoController';
import ConverterController from './controller/ConverterController';
import InfoService from './service/InfoService';
import logger from './lib/logger';

process.on('uncaughtException', (err) => {
  logger.error(err.stack);
});
const infoService = new InfoService({});
const converterService = new ConverterService();
const infoController = new InfoController({ infoService });
const converterController = new ConverterController({ converterService });
const infoRouter = new InfoRouter({ infoController });
const converterRouter = new ConverterRouter({ converterController });
const basicRouter = new BasicRouter({ infoRouter, converterRouter });
const app = new App({ basicRouter });
const httpServer = new HttpServer({ app });
httpServer.start();
