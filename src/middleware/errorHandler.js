import Promise from 'bluebird';
import shortid from 'shortid';
import logger from '../lib/logger';

import ValidationError from '../error/ValidationError';


export default () => (err, req, res, next) => { // eslint-disable-line no-unused-vars
  const failureId = shortid.generate();
  return Promise
    .reject(err)
    .catch(ValidationError, err => res.status(400).json({
      name: err.name,
      message: err.message,
      code: err.code,
      result: err.result,
      failureId,
    }))
    .then(() => {
      logger.warn({
        name: err.name,
        message: err.message,
        stack: err.stack,
        code: err.code,
        result: err.result,
        inconsistencies: err.inconsistencies,
        failureId,
      });
    })
    .catch((err) => {
      logger.error({
        name: err.name,
        message: err.message,
        stack: err.stack,
        code: err.code,
        failureId,
      });
      return res.status(500).json({
        name: err.name,
        message: err.message,
        code: err.code,
        failureId,
      });
    });
};
