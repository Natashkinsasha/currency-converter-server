import { Router } from 'express';

export default class BasicRouter extends Router {
  constructor({ infoRouter, converterRouter }) {
    super();
    infoRouter && this.use('/info', infoRouter); // eslint-disable-line no-unused-expressions
    converterRouter && this.use('/converter', converterRouter); // eslint-disable-line no-unused-expressions
  }
}
