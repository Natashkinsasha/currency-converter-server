import { Router } from 'express';
import ValidationError from '../error/ValidationError';
import errorCodes from '../error/errorCodes';

function validateConvert(req, res, next) {
  req.checkQuery({
    amount: {
      notEmpty: {
        errorMessage: errorCodes.REQUIRED,
      },
      isInt: {
        options: { gt: 0 },
        errorMessage: errorCodes.INVALID_NUMBER,
      },
    },
    from: {
      notEmpty: {
        errorMessage: errorCodes.REQUIRED,
      },
      isLength: {
        options: [{ min: 3, max: 3 }],
        errorMessage: errorCodes.INVALID_LENGTH,
      },
    },

    to: {
      notEmpty: {
        errorMessage: errorCodes.REQUIRED,
      },
      isLength: {
        options: [{ min: 3, max: 3 }],
        errorMessage: errorCodes.INVALID_LENGTH,
      },
    },
  });
  return req.getValidationResult()
    .then((result) => {
      if (result.isEmpty()) {
        return next();
      }
      throw new ValidationError({ result: result.array() });
    })
    .catch(next);
}

export default class ConverterRouter extends Router {
  constructor({ converterController }) {
    super();
    return this
      .get('/convert', validateConvert, converterController.convert);
  }
}
