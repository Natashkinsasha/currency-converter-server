import Promise from 'bluebird';
import config from 'node-config-env-value';
import CurrencyConverter from 'currency-converter';
import ValidationError from '../error/ValidationError';


export default class ConverterService {
  constructor() {
    const EXCHANGE_RATES_KEY = config.get('EXCHANGE_RATES_KEY');
    const FETCH_INTERVAL = config.get('FETCH_INTERVAL');
    this.currencyConverter = CurrencyConverter({ CLIENTKEY: EXCHANGE_RATES_KEY, fetchInterval: FETCH_INTERVAL });
  }

    convert = ({ amount, from, to }) => Promise
      .try(() => this.currencyConverter.convert(amount, from, to))
      .catch((err) => {
        if (!err) {
          throw new ValidationError({});
        }
      })
      .get('amount')
}
