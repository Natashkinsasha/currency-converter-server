import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import { SERVER_URL } from './util';

describe('Test info endpoint', () => {
  chai.use(chaiHttp);
  chai.use(dirtyChai);
  const { expect } = chai;

  describe('#GET /api/converter/convert', () => {
    it('should return number', (done) => {
      chai
        .request(SERVER_URL)
        .get('/api/converter/convert')
        .query({
          amount: 10,
          from: 'EUR',
          to: 'USD',
        })
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.have.property('body').to.be.a('number');
          done();
        })
        .catch(done);
    });
  });
});
