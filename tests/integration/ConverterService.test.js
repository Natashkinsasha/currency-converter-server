import chai from 'chai';
import dirtyChai from 'dirty-chai';
import ConverterService from '../../src/service/ConverterService';


describe('Test ConverterService', () => {
  chai.use(dirtyChai);
  const { expect } = chai;
  const converterService = new ConverterService();


  describe('#convert', () => {
    it('', (done) => {
      converterService
        .convert({ amount: 1, from: 'USD', to: 'EUR' })
        .then((result) => {
          expect(result).to.be.a('number');
          done();
        })
        .catch(done);
    });
  });
});
