import chai from 'chai';
import request from 'supertest';
import dirtyChai from 'dirty-chai';

import App from '../../src/app/App';
import BasicRouter from '../../src/router/BasicRouter';
import ConverterRouter from '../../src/router/ConverterRouter';
import ConverterController from '../../src/controller/ConverterController';
import ConverterService from '../../src/service/ConverterService';


describe('Test InfoRouter', () => {
  chai.use(dirtyChai);
  const { expect } = chai;
  const converterService = new ConverterService();
  const converterController = new ConverterController({ converterService });
  const converterRouter = new ConverterRouter({ converterController });
  const basicRouter = new BasicRouter({ converterRouter });
  const app = new App({ basicRouter });


  describe('#GET /api/converter/convert', () => {
    it('should return number', () =>
      request(app)
        .get('/api/converter/convert')
        .query({
          amount: 10,
          from: 'EUR',
          to: 'USD',
        })
        .expect(200)
        .then((res) => {
          expect(res).to.have.property('body').to.be.a('number');
        }));

    it('should return response with 400 code and ValidationError', () =>
      request(app)
        .get('/api/converter/convert')
        .query({
          from: 'EUR',
          to: 'USD',
        })
        .expect(400)
        .then((res) => {
          expect(res).to.have.property('body').to.be.a('object');
          expect(res.body).to.have.property('name', 'ValidationError').to.be.a('string');
          expect(res.body).to.have.property('message', 'Validation error').to.be.a('string');
          expect(res.body).to.have.property('code', 'VALIDATION_ERROR').to.be.a('string');
          expect(res.body).to.have.property('failureId');
          expect(res.body).to.have.property('result').to.be.a('array');
          expect(res.body.result).to.deep.include({ location: 'query', param: 'amount', msg: 'REQUIRED' });
        }));
  });
});
