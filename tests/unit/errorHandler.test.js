import chai from 'chai';
import sinon from 'sinon';
import shortid from 'shortid';
import dirtyChai from 'dirty-chai';

import errorHandler from '../../src/middleware/errorHandler';

describe('Test errorHandler', () => {
  chai.use(dirtyChai);
  const { expect } = chai;

  it('should calls status with 500 and json', (done) => {
    const res = {
      status: sinon.spy(() => res),
      json: sinon.spy(),
    };
    const message = shortid.generate();
    errorHandler()(new TypeError(message), undefined, res)
      .then(() => {
        expect(res.status.calledOnce).to.be.true();
        expect(res.status.calledWith(500)).to.be.true();
        expect(res.json.calledOnce).to.be.true();
        expect(res.json.getCall(0).args[0]).to.have.property('name', 'TypeError');
        expect(res.json.getCall(0).args[0]).to.have.property('message', message);
        expect(res.json.getCall(0).args[0]).to.have.property('code');
        expect(res.json.getCall(0).args[0]).to.have.property('failureId');
        done();
      })
      .catch(done);
  });
});
